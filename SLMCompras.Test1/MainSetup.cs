﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace SLMCompras.Test1
{
    [SetUpFixture]
    public class MainSetup
    {
        [OneTimeSetUp]
        public void Setup()
        {
            Effort.Provider.EffortProviderConfiguration.RegisterProvider();
        }
    }
}
