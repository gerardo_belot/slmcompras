﻿using NUnit.Framework;

using System.Linq;
using SLMCompras.Models;

namespace SLMCompras.Test1.Integretion
{
    [TestFixture]
    public class TestClass
    {
        [SetUp]
        public void Setup()
        {
            PFactoty.ResetDb();
            using (var context = new ComprasDataContext())
            {
                var ori1 = context.Proveedor.Add(new Proveedores
                {
                    nombre = "Juan Lopez",
                    direccion_factura = "Barrio Altos de la Hoya, Calle Las Mercedes, Casa 1167",
                    pais = "Honduras",
                    email = "juan@test.com",
                    tel = "3255-8855",
                    Terminos = new TerminosPago()
                    {
                        Titulo = "Contado"
                    }
                });

                var Contactos = context.ProveedoresContactoses.Add(new ProveedoresContactos()
                {
                    alias = "Sr",
                    nombre = "Carlos Albarenga",
                    tel = "3258-5688",
                    movil = "9988-7755",
                    cargo = "Gerente de ventas",
                    email = "calbarenga@test.com"
                });

                ori1.Contactos.Add(Contactos);

                context.SaveChanges();
            }
        }

        [Test]
        public void ProveedoresAceptaRelacionesConTerminosPago()
        {
            using (var c = new ComprasDataContext())
            {
                Assert.AreEqual(1, c.Proveedor.First().Terminos.Id);
                Assert.AreEqual("Contado", c.Proveedor.First().Terminos.Titulo);
            }
        }

        [Test]
        public void ProveedoresAceptaRelacionConProveedoresContactos()
        {
            using (var c = new ComprasDataContext())
            {
                Assert.AreEqual("Carlos Albarenga", c.Proveedor.First().Contactos.First().nombre);
            }
        }
    }
}