﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SLMCompras.Models;
using SLMCompras.Views;
using SLMCompras.Views.Items;
using SLMCompras.Views.Proveedores;

namespace SLMCompras
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void openProveedores(object sender, RoutedEventArgs e)
        {
            new ProveedoresView().ShowDialog();
        }

        private void openItems(object sender, RoutedEventArgs e)
        {
            new ItemsView().ShowDialog();
        }
    }
}
