﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SLMCompras.Models.Repositories
{
    public class ProveedoresRepository : Repository<Proveedores>
    {
        public new void Update(Proveedores repo)
        {
            Proveedores record = new Proveedores();
            record = Get(repo.id);
            record.nombre = repo.nombre;
            record.categoria = repo.categoria;
            record.pais = repo.pais;
            record.tel = repo.tel;
            record.tel_alt = repo.tel_alt;
            record.fax = repo.fax;
            record.portal = repo.portal;
            record.direccion_factura = repo.direccion_factura;
            record.Terminos_id = repo.Terminos_id;

            SaveChanges();
        }
    }
}
