﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SLMCompras.Models.Repositories
{
    public class Repository<T> where T : class
    {
        private ComprasDataContext context = new ComprasDataContext();

        protected DbSet<T> DbSet { get; set; }

        private bool disposed;

        public Repository()
        {
            DbSet = context.Set<T>();
        }

        public List<T> GetList()
        {
            return DbSet.ToList();
        }

        public T Get(int id)
        {
            return DbSet.Find(id);
        }

        public void Add(T entity)
        {
            DbSet.Add(entity);
        }

        public void Update(T entry)
        {
            context.Entry<T>(entry).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            DbSet.Remove(DbSet.Find(id));
        }

        public void SaveChanges()
        {
           System.Diagnostics.Debug.Print($"Morgan opina que la función de guardado de este Repositorio de tipo {GetType().Name} devolvió {context.SaveChanges()}!");
        }

        public void Dispose()
        {
            if (!disposed)
            {
                context.Dispose();
                disposed = true;
            }
        }
    }
}