﻿using System.Data.Entity;

namespace SLMCompras.Models
{
    public class ComprasDataContext : DbContext
    {
        public DbSet<Proveedores> Proveedor { get; set; }
        public DbSet<ProveedoresContactos> ProveedoresContactoses { get; set; }
        public DbSet<TerminosPago> TerminosPago { get; set; }
        public DbSet<Item> Items { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Proveedores>()
                .HasRequired<TerminosPago>(s => s.Terminos)
                .WithMany(g => g.Proveedores)
                .HasForeignKey<int>(s => s.Terminos_id);

            modelBuilder.Entity<ProveedoresContactos>()
                .HasRequired<Proveedores>(s => s.Proveedores)
                .WithMany(g => g.Contactos)
                .HasForeignKey<int>(s => s.Proveedores_id);
        }

    }


}