﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SLMCompras.Models
{
    public class TerminosPago
    {
        [Key()]
        public int Id { get; set; }

        [Required()]
        [StringLength(100, MinimumLength = 2)]
        public string Titulo { get; set; }

        public ICollection<Proveedores> Proveedores { get; set; }
    }
}
