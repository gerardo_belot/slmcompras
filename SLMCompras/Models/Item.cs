﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SLMCompras.Models
{
    public class Item
    {
        [Key()]
        public int Id { get; set; }

        public int Code { get; set; }

        public string descripcion { get; set; }

        public int unidades { get; set; }

    }
}
