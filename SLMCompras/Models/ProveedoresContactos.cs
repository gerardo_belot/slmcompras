﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SLMCompras.Models
{
    public class ProveedoresContactos
    {
        [Key()] public int ContactoId { get; set; }

        public string alias { get; set; }

        public string nombre { get; set; }

        public string tel { get; set; }

        public string ext { get; set; }

        public string movil { get; set; }

        public string cargo { get; set; }

        public string email { get; set; }

        public virtual Proveedores Proveedores { get; set; }

        public int Proveedores_id { get; set; }
    }
}