﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Windows.Documents;


namespace SLMCompras.Models
{
    public class Proveedores
    {
        [Key]
        public int id { get; set; }

        [Required()]
        [StringLength(150, MinimumLength = 3)]
        public string nombre { get; set; }

        [Required()]
        public string direccion_factura { get; set; }

        [StringLength(100)]
        public string email { get; set; }

        [Required()]
        public string categoria { get; set; }
        
        [Required()]
        public string pais { get; set; }

        [Required()]
        public string tel { get; set; }

        public string tel_alt { get; set; }

        public string fax { get; set; }

        public string tmovil { get; set; }

        public string departamento { get; set; }

        public string portal { get; set; }

        public int Terminos_id { get; set; }

        public virtual TerminosPago Terminos {get; set;}

        public virtual List<ProveedoresContactos> Contactos { get; set; }

    }
}