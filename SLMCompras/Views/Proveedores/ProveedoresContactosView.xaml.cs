﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SLMCompras.Models;

namespace SLMCompras.Views.Proveedores
{
    /// <summary>
    /// Interaction logic for ProveedoresContactosView.xaml
    /// </summary>
    public partial class ProveedoresContactosView : Window
    {
        private ProveedoresContactos _ProveedoresContactos;

        public ProveedoresContactosView()
        {
            InitializeComponent();
        }

        public ProveedoresContactos GetValue()
        {
            return (ShowDialog() ?? false) ? _ProveedoresContactos : null;
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            _ProveedoresContactos = new ProveedoresContactos()
            {
                alias = aliasTxt.Text,
                nombre = nombreTxt.Text,
                tel = telTxt.Text,
                ext = extTxt.Text,
                movil = movilTxt.Text,
                cargo = cargoTxt.Text
            };

            DialogResult = true;
            Close();
        }
    }
}
