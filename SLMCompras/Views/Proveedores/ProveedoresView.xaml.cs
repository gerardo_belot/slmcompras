﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SLMCompras.Models;
using SLMCompras.Models.Repositories;

namespace SLMCompras.Views.Proveedores
{
    /// <summary>
    /// Interaction logic for ProveedoresView.xaml
    /// </summary>
    public partial class ProveedoresView : Window
    {
        private Timer timesearch = new Timer(1000);
        private ProveedoresRepository repo = new ProveedoresRepository();


        public ProveedoresView()
        {
            InitializeComponent();
            cargarGrid();
            timesearch.Elapsed += Timesearch_Elapsed; 
        }

        private void Timesearch_Elapsed(object sender, ElapsedEventArgs e)
        {
            timesearch.Stop();
            Dispatcher.Invoke(() =>
            {

                string texto = searchTxt.Text;

                using (ComprasDataContext context = new ComprasDataContext())
                {
                   var sresults = context.Proveedor.Where(p => p.nombre.Contains(texto) || p.tel.Contains(texto));
                   DataGridData.ItemsSource = sresults.ToList();
                }
            });
        }

        private void cargarGrid()
        {
            var result = repo.GetList();
            DataGridData.ItemsSource = result;
        }

        private void Row_DoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (DataGridData.SelectedItem == null) return;
            var selectedPerson = DataGridData.SelectedItem as Models.Proveedores;
            ProveedoresEditView.getData(selectedPerson.id, cargarGrid);
        }
        

        private void openNuevoProveedor(object sender, RoutedEventArgs e)
        {
            new NuevoProveedor().ShowDialog();
        }

        private void searchOnKeyPrees(object sender, TextChangedEventArgs e)
        {
            //MessageBox.Show("Dice " + (sender as TextBox)?.Text);
            timesearch.Start();
        }
    }
}
