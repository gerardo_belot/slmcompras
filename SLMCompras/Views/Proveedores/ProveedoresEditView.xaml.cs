﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SLMCompras.Models.Repositories;
using SLMCompras.Models;

namespace SLMCompras.Views.Proveedores
{
    /// <summary>
    /// Interaction logic for ProveedoresEditView.xaml
    /// </summary>
    public partial class ProveedoresEditView : Window
    {
        private TerminosPagosRepository terminos = new TerminosPagosRepository();
        private ProveedoresRepository proveedores = new ProveedoresRepository();
        
        private Models.Proveedores repo;
        private int Local_Id;

        private ProveedoresEditView(int id)
        {
            InitializeComponent();
            repo = new ProveedoresRepository().Get(id);
            fillComponents(repo);
            cargarCombobox(repo);
            cargarCombox2(repo);
        }

        public static void getData(int id, Action onClose)
        {
            var x = new ProveedoresEditView(id);
            x.ShowDialog();
            onClose.Invoke();
        }

        private void cargarCombobox(Models.Proveedores repo)
        {
            var result = terminos.GetList();
            terminos_pago.ItemsSource = result;
            terminos_pago.SelectedIndex= repo.Terminos.Id -1;
        }

        private void cargarCombox2(Models.Proveedores repo)
        {
            categoria.Text = repo.categoria;
        }

        private void fillComponents(Models.Proveedores repo)
        {
            this.Local_Id = repo.id;
            this.nombre.Text = repo.nombre.ToString();
            this.pais.Text = repo.pais.ToString();
            this.tel.Text = repo.tel.ToString();
            this.tel_alt.Text = repo.tel_alt.ToString();
            this.fax.Text = repo.fax.ToString();
            this.portal.Text = repo.portal.ToString();
            this.direccion_factura.Text = repo.direccion_factura.ToString();
        }

        private void BtnEditOnClick(object sender, RoutedEventArgs e)
        {
            repo.id = this.Local_Id;
            repo.nombre = nombre.Text;
            repo.categoria = categoria.Text;
            repo.pais = pais.Text;
            repo.tel = tel.Text;
            repo.tel_alt = tel_alt.Text;
            repo.fax = fax.Text;
            repo.portal = portal.Text;
            repo.direccion_factura = direccion_factura.Text;
            repo.Terminos_id = terminos_pago.SelectedIndex + 1;

            proveedores.Update(repo);
            MessageBox.Show("El documentos se actualizo correctamente");
        }

        private void BtnCloseOnClick(object sender, StylusEventArgs e)
        {
            this.Close();
        }


    }
}
