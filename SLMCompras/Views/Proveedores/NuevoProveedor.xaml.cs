﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SLMCompras.Models;
using SLMCompras.Models.Repositories;

namespace SLMCompras.Views.Proveedores
{
    /// <summary>
    /// Interaction logic for NuevoProveedor.xaml
    /// </summary>
    public partial class NuevoProveedor : Window
    {
        private TerminosPagosRepository terminos = new TerminosPagosRepository();

        private ProveedoresRepository proveedores = new ProveedoresRepository();

        private List<ProveedoresContactos> contactosEnMemoria = new List<ProveedoresContactos>();

        public NuevoProveedor()
        {
            InitializeComponent();
            cargarCombobox();
            this.ContactosDataGrid.ItemsSource = contactosEnMemoria;
        }

        private void cargarCombobox()
        {
            var result = terminos.GetList();
            terminos_pago.ItemsSource = result;
        }

        private void cargarGrid()
        {
            this.ContactosDataGrid.ItemsSource = contactosEnMemoria;
        }

        private void BtnAdd_Click(object sender, RoutedEventArgs e)
        {

            var termino = terminos_pago.SelectedValue.ToString();
            Models.Proveedores proveedor = new Models.Proveedores()
            {
                nombre = nombre.Text,
                categoria = categoria.Text,
                pais = pais.Text,
                tel = tel.Text,
                tel_alt = tel_alt.Text,
                fax = fax.Text,
                portal = portal.Text,
                direccion_factura = direccion_factura.Text,
                Terminos_id = Int32.Parse(termino)
            };

            proveedores.Add(proveedor);
            proveedores.SaveChanges();
            clear();
            cargarCombobox();
            MessageBox.Show("El documentos se guardo correctamente");
        }

        public void clear()
        {
            nombre.Text = "";
            categoria.Text = "";
            pais.Text = "";
            tel.Text = "";
            tel_alt.Text = "";
            fax.Text = "";
            portal.Text = "";
            direccion_factura.Text = "";
        }

        private void BtnOpenterms(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("En construccion");
        }

        private void BtnCreateContacto(object sender, RoutedEventArgs e)
        {
            var result = new ProveedoresContactosView().GetValue();
            contactosEnMemoria.Add(result);
            cargarGrid();
            if (contactosEnMemoria != null)
            {
                contactosEnMemoria.ForEach(i => Console.WriteLine($"{i.nombre}\t"));
            }
            
        }
    }
}