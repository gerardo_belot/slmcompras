﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SLMCompras.Models;
using SLMCompras.Models.Repositories;

namespace SLMCompras.Views.Items
{
    /// <summary>
    /// Interaction logic for ItemsCreateView.xaml
    /// </summary>
    public partial class ItemsCreateView : Window
    {

        private ItemsRepository items = new ItemsRepository();

        public ItemsCreateView()
        {
            InitializeComponent();
        }

        private void BtnCreate(object sender, RoutedEventArgs e)
        {
            Models.Item item = new Item()
            {
                Code = Int32.Parse(codigoTxt.Text),
                descripcion = descricionTxt.Text,
                unidades = Int32.Parse(unidadesTxt.Text)
            };

            items.Add(item);
            items.SaveChanges();
            clear();
            MessageBox.Show("El registro a sido guardado");
        }

        private void clear()
        {
            codigoTxt.Text = "";
            descricionTxt.Text = "";
            unidadesTxt.Text = "";
        }

        private void BtnClose(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
