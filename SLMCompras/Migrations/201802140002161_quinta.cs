namespace SLMCompras.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class quinta : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ProveedoresContactosProveedores", "ProveedoresContactos_ContactoId", "dbo.ProveedoresContactos");
            DropForeignKey("dbo.ProveedoresContactosProveedores", "Proveedores_id", "dbo.Proveedores");
            DropIndex("dbo.ProveedoresContactosProveedores", new[] { "ProveedoresContactos_ContactoId" });
            DropIndex("dbo.ProveedoresContactosProveedores", new[] { "Proveedores_id" });
            AddColumn("dbo.ProveedoresContactos", "Proveedores_id", c => c.Int());
            CreateIndex("dbo.ProveedoresContactos", "Proveedores_id");
            AddForeignKey("dbo.ProveedoresContactos", "Proveedores_id", "dbo.Proveedores", "id");
            DropTable("dbo.ProveedoresContactosProveedores");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ProveedoresContactosProveedores",
                c => new
                    {
                        ProveedoresContactos_ContactoId = c.Int(nullable: false),
                        Proveedores_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ProveedoresContactos_ContactoId, t.Proveedores_id });
            
            DropForeignKey("dbo.ProveedoresContactos", "Proveedores_id", "dbo.Proveedores");
            DropIndex("dbo.ProveedoresContactos", new[] { "Proveedores_id" });
            DropColumn("dbo.ProveedoresContactos", "Proveedores_id");
            CreateIndex("dbo.ProveedoresContactosProveedores", "Proveedores_id");
            CreateIndex("dbo.ProveedoresContactosProveedores", "ProveedoresContactos_ContactoId");
            AddForeignKey("dbo.ProveedoresContactosProveedores", "Proveedores_id", "dbo.Proveedores", "id", cascadeDelete: true);
            AddForeignKey("dbo.ProveedoresContactosProveedores", "ProveedoresContactos_ContactoId", "dbo.ProveedoresContactos", "ContactoId", cascadeDelete: true);
        }
    }
}
