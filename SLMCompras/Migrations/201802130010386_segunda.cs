namespace SLMCompras.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class segunda : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Proveedores", "categoria", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Proveedores", "categoria", c => c.Int(nullable: false));
        }
    }
}
