namespace SLMCompras.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class tercera : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Proveedores", new[] { "Terminos_Id" });
            AlterColumn("dbo.Proveedores", "Terminos_id", c => c.Int(nullable: false));
            CreateIndex("dbo.Proveedores", "Terminos_Id");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Proveedores", new[] { "Terminos_Id" });
            AlterColumn("dbo.Proveedores", "Terminos_id", c => c.Int());
            CreateIndex("dbo.Proveedores", "Terminos_Id");
        }
    }
}
