namespace SLMCompras.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class cuarta3 : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.Proveedores", "Terminos_id");
            AddForeignKey("dbo.Proveedores", "Terminos_id", "dbo.TerminosPagoes", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Proveedores", "Terminos_id", "dbo.TerminosPagoes");
            DropIndex("dbo.Proveedores", new[] { "Terminos_id" });
        }
    }
}
