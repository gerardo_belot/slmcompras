namespace SLMCompras.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class cuarta4 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Proveedores", "codigo");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Proveedores", "codigo", c => c.String(nullable: false));
        }
    }
}
