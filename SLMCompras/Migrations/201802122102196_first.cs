namespace SLMCompras.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class first : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Items",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.Int(nullable: false),
                        descripcion = c.String(),
                        unidades = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Proveedores",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        codigo = c.String(nullable: false),
                        nombre = c.String(nullable: false, maxLength: 150),
                        direccion_factura = c.String(nullable: false),
                        email = c.String(maxLength: 100),
                        categoria = c.Int(nullable: false),
                        pais = c.String(nullable: false),
                        tel = c.String(nullable: false),
                        tel_alt = c.String(),
                        fax = c.String(),
                        tmovil = c.String(),
                        departamento = c.String(),
                        portal = c.String(),
                        Terminos_Id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.TerminosPagoes", t => t.Terminos_Id)
                .Index(t => t.Terminos_Id);
            
            CreateTable(
                "dbo.ProveedoresContactos",
                c => new
                    {
                        ContactoId = c.Int(nullable: false, identity: true),
                        alias = c.String(),
                        nombre = c.String(),
                        tel = c.String(),
                        movil = c.String(),
                        cargo = c.String(),
                        email = c.String(),
                    })
                .PrimaryKey(t => t.ContactoId);
            
            CreateTable(
                "dbo.TerminosPagoes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Titulo = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ProveedoresContactosProveedores",
                c => new
                    {
                        ProveedoresContactos_ContactoId = c.Int(nullable: false),
                        Proveedores_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ProveedoresContactos_ContactoId, t.Proveedores_id })
                .ForeignKey("dbo.ProveedoresContactos", t => t.ProveedoresContactos_ContactoId, cascadeDelete: true)
                .ForeignKey("dbo.Proveedores", t => t.Proveedores_id, cascadeDelete: true)
                .Index(t => t.ProveedoresContactos_ContactoId)
                .Index(t => t.Proveedores_id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Proveedores", "Terminos_Id", "dbo.TerminosPagoes");
            DropForeignKey("dbo.ProveedoresContactosProveedores", "Proveedores_id", "dbo.Proveedores");
            DropForeignKey("dbo.ProveedoresContactosProveedores", "ProveedoresContactos_ContactoId", "dbo.ProveedoresContactos");
            DropIndex("dbo.ProveedoresContactosProveedores", new[] { "Proveedores_id" });
            DropIndex("dbo.ProveedoresContactosProveedores", new[] { "ProveedoresContactos_ContactoId" });
            DropIndex("dbo.Proveedores", new[] { "Terminos_Id" });
            DropTable("dbo.ProveedoresContactosProveedores");
            DropTable("dbo.TerminosPagoes");
            DropTable("dbo.ProveedoresContactos");
            DropTable("dbo.Proveedores");
            DropTable("dbo.Items");
        }
    }
}
