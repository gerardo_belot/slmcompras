namespace SLMCompras.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class sexta : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ProveedoresContactos", "Proveedores_id", "dbo.Proveedores");
            DropIndex("dbo.ProveedoresContactos", new[] { "Proveedores_id" });
            AlterColumn("dbo.ProveedoresContactos", "Proveedores_id", c => c.Int(nullable: false));
            CreateIndex("dbo.ProveedoresContactos", "Proveedores_id");
            AddForeignKey("dbo.ProveedoresContactos", "Proveedores_id", "dbo.Proveedores", "id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProveedoresContactos", "Proveedores_id", "dbo.Proveedores");
            DropIndex("dbo.ProveedoresContactos", new[] { "Proveedores_id" });
            AlterColumn("dbo.ProveedoresContactos", "Proveedores_id", c => c.Int());
            CreateIndex("dbo.ProveedoresContactos", "Proveedores_id");
            AddForeignKey("dbo.ProveedoresContactos", "Proveedores_id", "dbo.Proveedores", "id");
        }
    }
}
