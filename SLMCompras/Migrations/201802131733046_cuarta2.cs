namespace SLMCompras.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class cuarta2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Proveedores", "Terminos_Id", "dbo.TerminosPagoes");
            DropIndex("dbo.Proveedores", new[] { "Terminos_Id" });
            AlterColumn("dbo.Proveedores", "Terminos_id", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Proveedores", "Terminos_id", c => c.Int());
            CreateIndex("dbo.Proveedores", "Terminos_Id");
            AddForeignKey("dbo.Proveedores", "Terminos_Id", "dbo.TerminosPagoes", "Id");
        }
    }
}
